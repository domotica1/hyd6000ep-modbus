import os
import time
from datetime import datetime

import minimalmodbus
import paho.mqtt.client as mqtt #pip3 install paho-mqtt
import influxdb_client 
from influxdb_client.client.write_api import SYNCHRONOUS


MQTT = True
INFLUX = True

if INFLUX is True:
  url = "http://localhost:8086"
  token = "PKBjxfdB9NOQtYqAc-JWQyvlf0DBJXv9QknJ2-lt3RsLlQuCKutGPsI9vKYZDI2txI_NaudJOZg0OVdikVUKTw=="
  org = "home"
  bucket = "fotovoltaica"
  client_influx = influxdb_client.InfluxDBClient(
    url=url,
    token=token,
    org=org
  )
  write_api = client_influx.write_api(write_options=SYNCHRONOUS)  

if MQTT is True:
  USER = os.environ.get('USER') or None
  PWD = os.environ.get('PWD') or None
  IP = "localhost"

  clientmqtt = mqtt.Client("", True, None, mqtt.MQTTv31)
  #clientmqtt.username_pw_set(USER,password=PWD)
  clientmqtt.connect(IP,1883,60)


#instrument = minimalmodbus.Instrument('/dev/ttyUSB0', 1)  # port name, slave address (in decimal)
#instrument.serial.baudrate = 9600
##instrument.debug = True
#instrument.serial.timeout  = 2
#instrument.close_port_after_each_call = False

dic_values = {}
#Register, decimals, Signed
dic_registers = {
  "consumo" : [1199, 2, False],
  "bat_soc" : [1544, 0, False],
  "bat_volt" : [1540, 1, False],
  "bat_temp" : [1543, 0, False],
  "bat_cycles" : [1546, 0, False],
  "bat_io" : [1542, 2, True],
  "stream1" : [1414, 2, False],
  "stream2" : [1417, 2, False],
  "t_internal" : [1048, 0, False],
  "t_heatsink" : [1050 , 0, False]
}

def read_modbus(values):
  register=values[0]
  decimals=values[1]
  signed=values[2]
  try:
    #return instrument.read_register(register, decimals, signed=signed)
    return None
  except IOError:
    print("Failed to read from instrument")
    return None


def write_data(key, value):
    if MQTT is True:
      clientmqtt.publish("FV/"+key, str(value) )
    if INFLUX is True:

      p = influxdb_client.Point(key).tag(key, key).field(key, value).time(datetime.utcnow(), WritePrecision.MS)
      write_api.write(bucket=bucket, org=org, record=p)

#      with InfluxDBClient(url=url, token=token, org=org) as client:
#        write_api = client.write_api(write_options=SYNCHRONOUS)
#        
#        point = Point(key) \
#          .tag(key, key) \
#          .field(key, value) \
#          .time(datetime.utcnow(), WritePrecision.MS)


while True:
  #READ MODBUS VALUES
  for key, value in dic_registers.items():
    returned_data = read_modbus(value)
    dic_values.update( { key : returned_data } )
    time.sleep (0.1)
    #instrument.serial.close()
    

    #WRITE DIC SPECIAL VALUES
    #dic_values.update( { "total_stream" : dic_values['stream1'] + dic_values['stream2'] } )

  #WRITE VALUES TO INFLUX & MQTT
  for key, value in dic_values.items():
    write_data(key, value)

  #if INFLUX is True:
  #  client.close()

  time.sleep(4)
  print ("Next Loop")

