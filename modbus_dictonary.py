import os
import time
from datetime import datetime

import minimalmodbus
import paho.mqtt.client as mqtt #pip3 install paho-mqtt
from influxdb_client.client.write_api import SYNCHRONOUS
from influxdb_client import InfluxDBClient, Point, WritePrecision

MQTT = False
INFLUX = True

if INFLUX is True:
  token = "PKBjxfdB9NOQtYqAc-JWQyvlf0DBJXv9QknJ2-lt3RsLlQuCKutGPsI9vKYZDI2txI_NaudJOZg0OVdikVUKTw=="
  org = "home"
  bucket = "fotovoltaica"

if MQTT is True:
  USER = os.environ.get('USER') or None
  PWD = os.environ.get('PWD') or None
  IP = "localhost"

  clientmqtt = mqtt.Client("", True, None, mqtt.MQTTv31)
  #clientmqtt.username_pw_set(USER,password=PWD)
  clientmqtt.connect(IP,1883,60)


instrument = minimalmodbus.Instrument('/dev/ttyUSB0', 1)  # port name, slave address (in decimal)
instrument.serial.baudrate = 9600
#instrument.debug = True
instrument.serial.timeout  = 2
instrument.close_port_after_each_call = False

dic_values = {}
#Register, decimals, Signed
dic_registers = {
  "consumo" : [1199, 2, False],
  "bat_soc" : [1544, 0, False],
  "bat_volt" : [1540, 1, False],
  "bat_temp" : [1543, 0, False],
  "bat_cycles" : [1546, 0, False],
  "bat_io" : [1542, 2, True],
  "stream1_V" :[1412, 1, False],
  "stream1_A" : [1413, 2, False],
  "stream1" : [1414, 2, False],
  "stream2_V" : [1415, 1, False], 
  "stream2_A" : [1416, 2, False],
  "stream2" : [1417, 2, False],
  "t_internal" : [1048, 0, False],
  "t_heatsink" : [1050 , 0, False],
  "operating_status" : [1028, 0, False],
  "active_power_output" : [1157, 2, True],
  "active_power_load_sys" : [1199, 2, False]
}

while True:
  #READ MODBUS VALUES
  for key in dic_registers:
    #print (key, '->', dic_registers[key])
    register = dic_registers[key][0]
    decimal = dic_registers[key][1]
    signed = dic_registers[key][2]
    try:
      dic_values.update( { key : instrument.read_register(register, decimal, signed=signed) } )
    except IOError:
      print("Failed to read from instrument")
    time.sleep (0.1)
  
  instrument.serial.close()

  #WRITE DIC SPECIAL VALUES
  dic_values.update( { "total_stream" : dic_values['stream1'] + dic_values['stream2'] } )
  dic_values.update( { "grid" : dic_values['active_power_load_sys'] - dic_values['active_power_output'] } )

  #WRITE VALUES TO INFLUX & MQTT
  for item in dic_values.keys():
#    print ("item: " + str(item) + " -> value: " + str(dic_values[item]))
 
    if MQTT is True:
      clientmqtt.publish("FV/"+item, str(dic_values[item]) )
    if INFLUX is True:
      date_now = datetime.utcnow()
      with InfluxDBClient(url="http://localhost:8086", token=token, org=org) as client:
        write_api = client.write_api(write_options=SYNCHRONOUS)
        
        point = Point(item) \
          .tag(item, item) \
          .field(item, float(dic_values[item])) \
          .time(date_now, WritePrecision.MS)

        write_api.write(bucket, org, point)

  if INFLUX is True:
    client.close()

  time.sleep(4)
  print ("Next Loop")
