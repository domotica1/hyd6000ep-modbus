
from pymodbus.client import ModbusTcpClient
from pymodbus.exceptions import ModbusException
from pymodbus.pdu import ExceptionResponse


client = ModbusTcpClient('eport-ee11.lan', 8899)


client.connect()

test = client.readwrite_registers(read_address=0x1010, read_count=1, write_address=0x1010, values=0, slave=1)

print (test)


"""

try:
    rr = client.read_holding_registers(0x1010, 1, slave=1)
except ModbusException as exc:
    print(f"Register : Received ModbusException({exc}) from library")
if rr.isError():
    print(f"Register  : Received Modbus library error({rr})")
if isinstance(rr, ExceptionResponse):
    print(f"Register  : Received Modbus library exception ({rr})")


if rr.isError() == False:
    print(rr.registers[0])
    print(rr)

print("go write")

try:
#write_register(address, value, slave=0)
    cw = client.write_register(0x1010, 0, slave=1)
except ModbusException as exc:
    print(f"Register : Received ModbusException({exc}) from library")
if rr.isError():
    print(f"Register  : Received Modbus library error({rr})")
if isinstance(rr, ExceptionResponse):
    print(f"Register  : Received Modbus library exception ({rr})")

if cw.isError() == False:
    print(cw.registers[0])
    print(cw)
else:
    print("error writing")

print("go read")

try:
    rr = client.read_holding_registers(0x1010, 1, slave=1)
except ModbusException as exc:
    print(f"Register : Received ModbusException({exc}) from library")
if rr.isError():
    print(f"Register  : Received Modbus library error({rr})")
if isinstance(rr, ExceptionResponse):
    print(f"Register  : Received Modbus library exception ({rr})")

if rr.isError() == False:
    print(rr.registers[0])
    print(rr)

"""