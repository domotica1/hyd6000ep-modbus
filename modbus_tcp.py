from dotenv import load_dotenv
import os
import time
from datetime import datetime

from pymodbus.client import ModbusTcpClient
from pymodbus.exceptions import ModbusException
from pymodbus.pdu import ExceptionResponse

import paho.mqtt.client as mqtt #pip3 install paho-mqtt
from influxdb_client.client.write_api import SYNCHRONOUS
from influxdb_client import InfluxDBClient, Point, WritePrecision

load_dotenv()

if os.environ.get('MQTT').lower() == 'true' :
  MQTT=True

if os.environ.get('INFLUX').lower() == 'true' :
  INFLUX=True

if INFLUX is True:
  print("entro dentro")
  INFLUX_TOKEN = os.environ.get('INFLUX_TOKEN') or None
  INFLUX_ORG = os.environ.get('INFLUX_ORG') or None
  INFLUX_BUCKET = os.environ.get('INFLUX_BUCKET') or None
  INFLUX_URL = os.environ.get('INFLUX_URL') or "http://localhost:8086"

if MQTT is True:
  MQTT_USER = os.environ.get('MQTT_USER') or None
  MQTT_PWD = os.environ.get('MQTT_PWD') or None
  MQTT_IP = os.environ.get('MQTT_IP') or "localhost"

  clientmqtt = mqtt.Client("", True, None, mqtt.MQTTv31)
  if MQTT_PWD is not None:
    clientmqtt.username_pw_set(MQTT_USER,password=MQTT_PWD)
  else:
    clientmqtt.connect(MQTT_IP,1883,60)


dic_values = {}
#Register, decimals, Signed
dic_registers = {
  "consumo" : [1199, 100, False],
  "bat_soc" : [1544, 1, False],
  "bat_volt" : [1540, 10, False],
  "bat_temp" : [1543, 1, False],
  "bat_cycles" : [1546, 1, False],
  "bat_io" : [1542, 100, True],
  "stream1_V" :[1412, 10, False],
  "stream1_A" : [1413, 100, False],
  "stream1" : [1414, 100, False],
  "stream2_V" : [1415, 10, False], 
  "stream2_A" : [1416, 100, False],
  "stream2" : [1417, 100, False],
  "t_internal" : [1048, 1, False],
  "t_heatsink" : [1050 , 1, False],
  "operating_status" : [1028, 1, False],
  "active_power_output" : [1157, 100, True],
  "active_power_load_sys" : [1199, 100, False],
  "today_export" : [1681, 100, False],
  "today_consumption" : [1688, 100, False],
  "today_import" : [1677, 100, False],
  "today_battery_discharge" : [1689, 100, False],
  "today_battery_charge" : [1685, 100, False],
  "today_generation" : [1669, 100, False]
}


def read_values(register, decimal, signed):
    try:
      rr = client_modbus.read_holding_registers(register, 1, slave=1)
      if rr.isError():
        print(f"Register {key} {register} : Received Modbus library error({rr})")

      else:

        if (signed is False):
          dic_values.update( { key : rr.registers[0]/decimal } )
        elif (signed is True):
          # si valor > 32767 -> valor - 65535
          # si valor < 32767 -> valor = valor
          if rr.registers[0] < 32767:
            dic_values.update( { key : rr.registers[0]/decimal } )
          else:
            dic_values.update( { key : (rr.registers[0] - 65535) /decimal } )
        else:
          print ("new case, debug it")

      if isinstance(rr, ExceptionResponse):
        print(f"Register {key} {register} : Received Modbus library exception ({rr})")

    except ModbusException as exc:
      print(f"Register {key} {register} : Received ModbusException({exc}) from library")



client_modbus = ModbusTcpClient('eport-ee11.lan', 8899)


while True:

  time_now = datetime.now()
  if time_now.hour == 23 and time_now.minute == 59 and time_now.second > 45:
    print ("sleep for reset values")
    time.sleep (30)

  client_modbus.connect()

  #READ MODBUS VALUES
  for key in dic_registers:
    #print (key, '->', dic_registers[key])
    register = dic_registers[key][0]
    decimal = dic_registers[key][1]
    signed = dic_registers[key][2]

    read_values(register, decimal, signed)

    time.sleep (0.1)

  client_modbus.close()

  #WRITE DIC SPECIAL VALUES
  if 'stream1' in dic_values and 'stream2' in dic_values:
    dic_values.update( { "total_stream" : dic_values['stream1'] + dic_values['stream2'] } )
  if 'active_power_load_sys' in dic_values and 'active_power_output' in dic_values:
    dic_values.update( { "grid" : dic_values['active_power_load_sys'] - dic_values['active_power_output'] } )

  #WRITE VALUES TO INFLUX & MQTT
  for item in dic_values.keys():
  #  print ("item: " + str(item) + " -> value: " + str(dic_values[item]))

    if MQTT is True:
      clientmqtt.publish("inverter/"+item, str(dic_values[item]) )
    if INFLUX is True:
      date_now = datetime.utcnow()
      with InfluxDBClient(url=INFLUX_URL, token=INFLUX_TOKEN, org=INFLUX_ORG) as client_influx:
        write_api = client_influx.write_api(write_options=SYNCHRONOUS)
        
        """ original
        point = Point(item) \
          .tag(item, item) \
          .field(item, float(dic_values[item])) \
          .time(date_now, WritePrecision.MS) """

        point = Point(item) \
          .field("value", float(dic_values[item])) \
          .time(date_now, WritePrecision.MS)

        write_api.write(INFLUX_BUCKET, INFLUX_ORG, point)

  if INFLUX is True:
    client_influx.close()


  time.sleep(4)


  print ("\nNext Loop\n")


