#!/usr/bin/env python3
import minimalmodbus, time

instrument = minimalmodbus.Instrument('/dev/ttyUSB0', 1)  # port name, slave address (in decimal)
instrument.serial.baudrate = 9600
#instrument.debug = True
instrument.serial.timeout  = 2

#print (instrument)
## Read temperature (PV = ProcessValue) ##

consumo = instrument.read_register(1199, 2)  # Registernumber, number of decimals
print("consumo: " + str(consumo) + " KWh")
time.sleep (0.25)
bat_soc = instrument.read_register(1544, 0)
print("bat_soc: " + str(bat_soc) + " %")
time.sleep (0.25)
bat_volt = instrument.read_register(1540, 1)
print("bat_volt: " + str(bat_volt) + " V")
time.sleep (0.25)
bat_temp = instrument.read_register(1543, 0)
print("bat_temp: " + str(bat_temp) + " Cº")
time.sleep (0.25)
bat_cycles = instrument.read_register(1546, 0)
print("bat_cycles: " + str(bat_cycles))
time.sleep (0.25)

bat_io = instrument.read_register(1542, 0)
print("bat_io: " + str(bat_io))
time.sleep (0.25)

stream1 = instrument.read_register(1414, 2)
print("stream1: " + str(stream1) + " KWh")
time.sleep (0.25)

stream2 = instrument.read_register(1417, 2)
print("stream2: " + str(stream2) + " KWh")
time.sleep (0.25)

print("total streams: " + str(stream2+stream1) + " KWh")

print("bat discharging: " + str((65535-bat_io)/100) + " KWh")
#test = instrument.read_register(int("430",16), 0)
#print ("test: " + str(test))



## Change temperature setpoint (SP) ##
#NEW_TEMPERATURE = 95
#instrument.write_register(24, NEW_TEMPERATURE, 1)  # Registernumber, value, number of decimals for storage
